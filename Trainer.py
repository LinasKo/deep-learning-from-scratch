import numpy as np
from mnist import MNIST
from layers.LinearLayer import LinearLayer
from layers.Sigmoid import Sigmoid
from layers.QuadraticLoss import QuadraticLoss


class Trainer:
    def __init__(self, layers, epochs, learning_rate, loss_function, stat_frequency=1):
        self.layers = layers
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.loss_f = loss_function
        self.stat_freq = stat_frequency

    def train(self, x, y, valid_x, valid_y):
        input_layer = LinearLayer(None, None, None)
        self.layers.insert(0, input_layer)

        for e in range(1, self.epochs+1):
            weight_grads = []
            bias_grads = []
            total_loss = 0
            total_correct = 0

            for x0, y0 in zip(x.copy(), y.copy()):
                input_layer.a = x0

                # Forward propagation
                for l in range(1, len(self.layers)):
                    self.layers[l].fprop(self.layers[l-1].a)

                # Compute Loss
                loss = self.loss_f.fprop(self.layers[-1].a, y0)
                total_loss += loss

                # Compute accuracy
                total_correct += y0[np.argmax(self.layers[-1].a)] == 1

                # Back propagation - last layer
                l_last = self.layers[-1]
                l_last.delta = self.loss_f.bprop(l_last.a, y0) * l_last.a_f.bprop(l_last.z)
                l_last.dw = np.outer(l_last.delta, self.layers[-2].a)
                l_last.db = l_last.delta

                # Back propagation - other layers
                for l in range(len(self.layers)-2, 0, -1):
                    self.layers[l].delta = np.dot(self.layers[l+1].w.T, self.layers[l+1].delta) * \
                                           self.layers[l].a_f.bprop(self.layers[l].z)
                    self.layers[l].dw = np.outer(self.layers[l].delta, self.layers[l-1].a)
                    self.layers[l].db = self.layers[l].delta

                # Gather all weights
                weight_grads.append([l.dw for l in self.layers[1:]])
                bias_grads.append([l.db for l in self.layers[1:]])

            # Gradient update
            mean_weight_grads = np.mean(np.array(weight_grads), axis=0)
            mean_bias_grads = np.mean(np.array(bias_grads), axis=0)

            for l in range(1, len(self.layers)):
                self.layers[l].w -= learning_rate * mean_weight_grads[l-1]
                self.layers[l].b -= learning_rate * mean_bias_grads[l-1]

            # Output loss, perform validation, and output validation loss
            if e % self.stat_freq == 0:
                print("[TRAIN] epoch %d: err %.3f, acc %.3f" % (e, total_loss, total_correct / (len(y))))

                total_valid_loss = 0
                total_valid_correct = 0
                for x0, y0 in zip(valid_x.copy(), valid_y.copy()):
                    input_layer.a = x0

                    # Forward propagation
                    for l in range(1, len(self.layers)):
                        self.layers[l].fprop(self.layers[l - 1].a)

                    # Compute Loss
                    loss = self.loss_f.fprop(self.layers[-1].a, y0)
                    total_valid_loss += loss

                    # Compute accuracy
                    total_valid_correct += y0[np.argmax(self.layers[-1].a)] == 1

                print("[VALID] epoch %d: err %.3f, acc %.3f" % (e, total_valid_loss, total_valid_correct / (len(valid_y))))

    @staticmethod
    def normalise(x, debug=False):
        mean = np.mean(x, axis=0)
        if debug:
            print("mean:\n", mean)
        x0 = x - mean
        if debug:
            print("zero mean:\n", x0)

        std = np.std(x, axis=0)
        if debug:
            print("std:\n", std)
        x_norm = x0 / (std + 1e-6)

        if debug:
            print("normalised:\n", x_norm)

        return x_norm


def to_one_hot(labels):
    res = []
    for label in labels:
        new_label = [0.] * 10
        new_label[label] = 1.
        res.append(np.array(new_label))
    return np.array(res)

if __name__ == "__main__":
    # Load data
    mndata = MNIST('./datasets/mnist')
    mndata.load_training()
    # mndata.load_testing()

    # train_images = np.array(mndata.train_images[:50000])
    # train_labels = np.array(mndata.train_labels[:50000])
    # valid_images = np.array(mndata.train_images[50000:60000])
    # valid_labels = np.array(mndata.train_labels[50000:60000])
    train_images = np.array(mndata.train_images[:500])
    train_labels = np.array(mndata.train_labels[:500])
    train_labels = to_one_hot(train_labels)
    valid_images = np.array(mndata.train_images[50000:50100])
    valid_labels = np.array(mndata.train_labels[50000:50100])
    valid_labels = to_one_hot(valid_labels)
    # test_images = np.array(mndata.test_images)
    # test_labels = np.array(mndata.test_labels)
    print("Finished loading dataset")

    # Set training hyperparameters
    epochs = 100
    learning_rate = 0.3
    quadratic_loss = QuadraticLoss()
    validation_rate = 1

    activation = Sigmoid()
    layers = [
        LinearLayer(
            np.random.random((256, 784)),
            np.zeros((256, )),
            activation
        ),
        LinearLayer(
            np.random.random((10, 256)),
            np.zeros((10, )),
            activation
        )
    ]

    trainer = Trainer(layers, epochs, learning_rate, quadratic_loss, validation_rate)
    train_images_norm = trainer.normalise(train_images)
    valid_images_norm = trainer.normalise(valid_images)
    trainer.train(train_images_norm, train_labels, valid_images_norm, valid_labels)
