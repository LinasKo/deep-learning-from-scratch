import numpy as np


class LinearLayer:
    def __init__(self, weights, biases, activation):
        self.w = weights
        self.b = biases
        self.a_f = activation
        self.z = None
        self.a = None
        self.delta = None
        self.dw = [0]
        self.db = None

    def fprop(self, x):
        self.z = self.w.dot(x) + self.b
        self.a = self.a_f.fprop(self.z)
