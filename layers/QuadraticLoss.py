import numpy as np


class QuadraticLoss:
    def fprop(self, a, y):
        quadratic_diff = (a - y) ** 2
        average_loss = np.average(quadratic_diff, axis=0)
        return average_loss

    def bprop(self, a, y):
        return 2. * (a - y)
