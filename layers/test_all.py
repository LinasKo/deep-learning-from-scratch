import numpy as np
from Trainer import *
from layers import LinearLayer, Sigmoid, QuadraticLoss

x = np.array([[1, 9, 7, 8, 3],
              [8, 9, 9, 1, 1],
              [8, 6, 3, 2, 1],
              [7, 8, 9, 3, 3],
              [4, 4, 4, 4, 4],
              [1, 3, 3, 9, 9],
              [7, 7, 5, 6, 4]])
y = np.array([[0, 1],
              [0, 1],
              [1, 0],
              [1, 0],
              [0, 1],
              [1, 0],
              [1, 0]])

x0 = Trainer.normalise(x, debug=True)[0]

w = np.random.random((8, 5))
print("weights:\n", w)

b = np.zeros((8,))
print("biases:\n", b)

a = Sigmoid.Sigmoid()

wTx = w.dot(x0)
print("wTx:\n", wTx)

l1 = LinearLayer.LinearLayer(weights=w, biases=b, activation=a)

print("x0 type:", type(x0))
print("x0 shape:", x0.shape)
l1.fprop(x0)
print("l1.z:\n", l1.z)
print("l1.a:\n", l1.a)

w2 = np.random.random((2, 8))
b2 = np.zeros((2,))
act2 = Sigmoid.Sigmoid()
l2 = LinearLayer.LinearLayer(weights=w2, biases=b2, activation=act2)

print("l1.a type:", type(l1.a))
print("l1.a shape:", l1.a.shape)
l2.fprop(l1.a)
print("l2.z:\n", l2.z)
print("l2.a:\n", l2.a)

maxed = y[0][np.argmax(l2.a)] == 1
print("actual:\n", y[0])
print("predicted:\n", maxed)

loss_f = QuadraticLoss.QuadraticLoss()
loss = loss_f.fprop(l2.a, y[0])
print("loss:\n", loss)

d_loss_d_a2 = loss_f.bprop(l2.a, y[0])
print("d_loss_d_a2:\n", d_loss_d_a2)

l2.delta = loss_f.bprop(l2.a, y[0]) * l2.a_f.bprop(l2.z)
l2.dw = np.outer(l2.delta, l1.a)
l2.db = l2.delta
print("l2.nabla:\n", l2.delta)
print("l2.dw:\n", l2.dw)
print("l2.db:\n", l2.db)

l1.delta = l2.w.T.dot(l2.delta) * l1.a_f.bprop(l1.z)
l1.dw = np.outer(l1.delta, x0)
l1.db = l1.delta
print("l1.delta:\n", l1.delta)
print("l1.dw:\n", l1.dw)
print("l1.db:\n", l1.db)

