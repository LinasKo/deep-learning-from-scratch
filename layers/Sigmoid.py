import numpy as np


class Sigmoid:
    def fprop(self, x):
        res = 1. / (1. + np.exp(-x))
        return res

    def bprop(self, x):
        sigmoid = self.fprop(x)
        res = sigmoid * (1. - sigmoid)
        return res